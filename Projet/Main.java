package Projet;

import Projet.Pieces.Piece;
import Projet.Pieces.Vehicule;
import Projet.Plateau.*;
import java.util.List;
import java.util.Scanner;
import java.util.Random;
import java.io.*;

public class Main {
    static Plateau plateauCharge = null;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Plateau plateau = null;

        System.out.println("Bienvenue dans le jeu !");
        System.out.println("Choisissez une option :");
        System.out.println("1. Joueur contre Joueur");
        System.out.println("2. Joueur contre Ordinateur (coups aléatoires)");
        System.out.println("3. Ordinateur contre Ordinateur (coups aléatoires)");
        System.out.println("4. Ouvrir une partie sauvegardée");
        System.out.print("Votre choix : ");

        int choix = scanner.nextInt();

        switch (choix) {
            case 1:
                jouerJoueurVsJoueur(plateau);
                break;
            case 2:
                jouerJoueurVsOrdinateur(plateau);
                break;
            case 3:
                jouerOrdinateurVsOrdinateur();
                break;
            case 4: 
                System.out.print("Entrez le nom du fichier de sauvegarde : ");
                scanner.nextLine(); 
                String nomFichier = scanner.nextLine();
                plateauCharge = chargerPartie(nomFichier);
                    if (plateauCharge != null) {
                        System.out.println("La partie a été chargée avec succès !");
                        System.out.println("Dans quelle mode de jeu ? JvsJ = 1, JvsO = 2");
                        int choix2 = scanner.nextInt();
                        switch (choix2) {
                            case 1:
                                jouerJoueurVsJoueur(plateauCharge);
                                break;
                            case 2:
                                jouerJoueurVsOrdinateur(plateauCharge);
                                break;
                        }
                    } else {
                        System.out.println("Impossible de charger la partie. Vérifiez le nom du fichier.");
                    }
                    break;
            default:
                System.out.println("Choix invalide. Fin du programme.");
                break;
        }

        scanner.close();
    }

    public static void jouerJoueurVsJoueur(Plateau plateauCharge) {
    Plateau plateau;
    Joueur joueur2;
    Joueur joueur1;
    if (plateauCharge != null) {
        plateau = plateauCharge;
        joueur1=plateau.getJoueur1();
        joueur2=plateau.getJoueur2();
    } else {
        plateau = new Plateau(16, 16);
        joueur1 = new Joueur("Joueur 1");
        joueur2 = new Joueur("Joueur 2");

        plateau.ajouterPieces(joueur1, joueur2);
    }

    System.out.println("Plateau initial :");
    plateau.afficherPlateau();
    System.out.println();

    Scanner scanner = new Scanner(System.in);
    while (true) {
        System.out.println("Tour du Joueur 1");
        plateau.tourDeJeu(joueur1, joueur2, scanner);

        System.out.println("Plateau après le tour du J1");
        plateau.afficherPlateau();
        System.out.println();

        int nuagesDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
        int nuagesDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
        if (plateau.VerifNuagesCollectes() || nuagesDansVehiculesJoueur1 > 24 || nuagesDansVehiculesJoueur2 > 24) {
            if (nuagesDansVehiculesJoueur1 > nuagesDansVehiculesJoueur2) {
                System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
            } else if (nuagesDansVehiculesJoueur1 < nuagesDansVehiculesJoueur2) {
                System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
            } else {
                System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
            }
            plateau.afficherCoupsJoues();
            break;
        }

        System.out.println("Tour du Joueur 2");
        plateau.tourDeJeu(joueur2, joueur1, scanner);

        plateau.deplacerNuagesAleatoirement();

        System.out.println("Plateau après le tour du Joueur 2 :");
        plateau.afficherPlateau();
        System.out.println();

        nuagesDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
        nuagesDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
        if (plateau.VerifNuagesCollectes() || nuagesDansVehiculesJoueur1 > 24 || nuagesDansVehiculesJoueur2 > 24) {
            if (nuagesDansVehiculesJoueur1 > nuagesDansVehiculesJoueur2) {
                System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
            } else if (nuagesDansVehiculesJoueur1 < nuagesDansVehiculesJoueur2) {
                System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
            } else {
                System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
            }
            plateau.afficherCoupsJoues();
            break;
        }
        System.out.println("1-Sauvegarder et stopper");
        System.out.println("2-Continuer");
        int sauvegardes = scanner.nextInt();
        if (sauvegardes == 1) {
            sauvegarderPartie(plateau, "Partie1");
            break;
        }
    }
    scanner.close();
}



    public static void jouerJoueurVsOrdinateur(Plateau plateauCharge) {
        Plateau plateau;
        Joueur joueur2;
        Joueur joueur1;
        if (plateauCharge != null) {
            plateau = plateauCharge;
            joueur1=plateau.getJoueur1();
            joueur2=plateau.getJoueur2();
        } else {
            plateau = new Plateau(16, 16);
            joueur1 = new Joueur("Joueur 1");
            joueur2 = new Joueur("Joueur 2");

            plateau.ajouterPieces(joueur1, joueur2);
        }

        System.out.println("Plateau initial :");
        plateau.afficherPlateau();
        System.out.println();

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        while (true) {
            System.out.println("Tour du Joueur 1");
            plateau.tourDeJeuOrdi(joueur1, joueur2, scanner);

            System.out.println("Plateau après le tour du Joueur 1 :");
            plateau.afficherPlateau();
            System.out.println();

            int nuageDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
            int nuageDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
            if (plateau.VerifNuagesCollectes() || nuageDansVehiculesJoueur1 > 24 || nuageDansVehiculesJoueur2 > 24) {
                if (nuageDansVehiculesJoueur1 > nuageDansVehiculesJoueur2) {
                    System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
                } else if (nuageDansVehiculesJoueur1 < nuageDansVehiculesJoueur2) {
                    System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
                } else {
                    System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
                }
                plateau.afficherCoupsJoues();
                break;
            }

            System.out.println("Tour du Joueur 2 (Ordinateur)");
        List<Piece> piecesJoueur2 = joueur2.AvoirPieces();
        Piece pieceAleatoire = piecesJoueur2.get(random.nextInt(piecesJoueur2.size()));

        Position nouvellePosition = null;
        do {
            int newX = random.nextInt(16);
            int newY = random.nextInt(16);
            nouvellePosition = new Position(newX, newY);
        if (plateau.VerifDeplacement(pieceAleatoire, nouvellePosition)) {
            plateau.deplacerPiece(pieceAleatoire, nouvellePosition, joueur2.AvoirPieces(), joueur1.AvoirPieces());
            break;
        }
        } while (true);

        System.out.println("Le Joueur 2 a déplacé la pièce " + pieceAleatoire + " en " + nouvellePosition);

        plateau.deplacerNuagesAleatoirement();

        System.out.println("Plateau après le tour du Joueur 2 :");
        plateau.afficherPlateau();
        System.out.println();

        nuageDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
        nuageDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
        if (plateau.VerifNuagesCollectes() || nuageDansVehiculesJoueur1 > 24 || nuageDansVehiculesJoueur2 > 24) {
            if (nuageDansVehiculesJoueur1 > nuageDansVehiculesJoueur2) {
                System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
            } else if (nuageDansVehiculesJoueur1 < nuageDansVehiculesJoueur2) {
                System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
            } else {
                System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
            }
                plateau.afficherCoupsJoues();
                break;
            }
        System.out.println("1-Sauvegarder et stopper");
        System.out.println("2-Continuer");
        int sauvegarde = scanner.nextInt();
        if (sauvegarde==1){
            sauvegarderPartie(plateau,"Partie1");
            break;
        }


    }
    scanner.close();
}
    
    public static void jouerOrdinateurVsOrdinateur() {
    Plateau plateau = new Plateau(16, 16);
    Joueur joueur1 = new Joueur("Joueur 1");
    Joueur joueur2 = new Joueur("Joueur 2");

    plateau.ajouterPieces(joueur1, joueur2);

    System.out.println("Plateau initial :");
    plateau.afficherPlateau();
    System.out.println();

    Scanner scanner = new Scanner(System.in);
    Random random = new Random();

    while (true) {
        System.out.println("Tour du Joueur 1");
        List<Piece> piecesJoueur1 = joueur1.AvoirPieces();
        Piece pieceAleatoire = piecesJoueur1.get(random.nextInt(piecesJoueur1.size()));

        Position nouvellePosition = null;
        do {
            int newX = random.nextInt(16);
            int newY = random.nextInt(16);
            nouvellePosition = new Position(newX, newY);
            if (plateau.VerifDeplacement(pieceAleatoire, nouvellePosition)) {
                plateau.deplacerPiece(pieceAleatoire, nouvellePosition, joueur1.AvoirPieces(), joueur1.AvoirPieces());
                break;
            }
        } while (true);

        String positionPieceStr = plateau.convertirPosition(nouvellePosition.getX(),nouvellePosition.getY());
        System.out.println("Le Joueur 1 a déplacé la pièce " + pieceAleatoire + " en " + positionPieceStr);

        System.out.println("Plateau après le tour du Joueur 1 :");
        plateau.afficherPlateau();
        System.out.println();

        int nuageeDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
        int nuageeDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
        if (plateau.VerifNuagesCollectes() || nuageeDansVehiculesJoueur1 > 24 || nuageeDansVehiculesJoueur2 > 24) {
            if (nuageeDansVehiculesJoueur1 > nuageeDansVehiculesJoueur2) {
                System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
            } else if (nuageeDansVehiculesJoueur1 < nuageeDansVehiculesJoueur2) {
                System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
            } else {
                System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
            }
            plateau.afficherCoupsJoues();
            break;
            }

        System.out.println("Tour du Joueur 2 (Ordinateur)");
        List<Piece> piecesJoueur2 = joueur2.AvoirPieces();
        Piece pieceAleatoire2 = piecesJoueur2.get(random.nextInt(piecesJoueur2.size()));

        Position nouvellePosition2 = null;
        do {
            int newX = random.nextInt(16);
            int newY = random.nextInt(16);
            nouvellePosition2 = new Position(newX, newY);
            if (plateau.VerifDeplacement(pieceAleatoire2, nouvellePosition2)) {
                plateau.deplacerPiece(pieceAleatoire2, nouvellePosition2, joueur2.AvoirPieces(), joueur1.AvoirPieces());
                break;
            }
        } while (true);

        String positionPieceStr2 = plateau.convertirPosition(nouvellePosition2.getX(),nouvellePosition2.getY());
        System.out.println("Le Joueur 2 a déplacé la pièce " + pieceAleatoire2 + " en " + positionPieceStr2);

        System.out.println("Plateau après le tour du Joueur 2 :");
        plateau.afficherPlateau();
        System.out.println();

        nuageeDansVehiculesJoueur1 = joueur1.compterNuagesDansVehicules();
        nuageeDansVehiculesJoueur2 = joueur2.compterNuagesDansVehicules();
        if (plateau.VerifNuagesCollectes() || nuageeDansVehiculesJoueur1 > 24 || nuageeDansVehiculesJoueur2 > 24) {
            if (nuageeDansVehiculesJoueur1 > nuageeDansVehiculesJoueur2) {
                System.out.println("Le Joueur 1 a collecté le plus de nuages et a gagné !");
            } else if (nuageeDansVehiculesJoueur1 < nuageeDansVehiculesJoueur2) {
                System.out.println("Le Joueur 2 a collecté le plus de nuages et a gagné !");
            } else {
                System.out.println("Égalité parfaite ! Bravo aux deux joueurs.");
            }
            plateau.afficherCoupsJoues();
            break;
            }
    }
    
    scanner.close();
}

    public static void sauvegarderPartie(Plateau plateau, String nomFichier) {
    try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(nomFichier))) {
        outputStream.writeObject(plateau);
        System.out.println("La partie a été sauvegardée avec succès dans " + nomFichier);
    } catch (IOException e) {
        System.err.println("Erreur lors de la sauvegarde de la partie : " + e.getMessage());
        e.printStackTrace();
    }
    }

    public static Plateau chargerPartie(String nomFichier) {
        Plateau plateau = null;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(nomFichier))) {
            plateau = (Plateau) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Erreur lors du chargement de la partie : " + e.getMessage());
        }
        return plateau;
    }
}







