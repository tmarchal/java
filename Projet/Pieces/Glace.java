package Projet.Pieces;

import Projet.Plateau.*;
import Projet.Pieces.*;
import java.io.Serializable;

public class Glace extends Piece implements Serializable {
    private static final long serialVersionUID = 1L;
    public Glace(String symbole, Position position) {
        super(symbole, position);
    }

    @Override
    public void deplacer(Position nouvellePosition) {
        this.position = nouvellePosition;
    }

    @Override
    public String toString() {
        return "GL"; 
    }
}







