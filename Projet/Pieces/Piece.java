package Projet.Pieces;

import Projet.Plateau.Position;
import java.io.Serializable;

public abstract class Piece implements Serializable {
    private static final long serialVersionUID = 1L;
    protected String type;
    protected Position position;

    public Piece(String type, Position position) {
        this.type = type;
        this.position = position;
    }

    public void deplacer(Position nouvellePosition) {
    this.position = nouvellePosition;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getType() {
        return type;
    }

}




