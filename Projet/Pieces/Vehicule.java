package Projet.Pieces;

import Projet.Plateau.Position;

public class Vehicule extends Piece {
    private String type; 
    private int nuagesCaptures; 

    public Vehicule(String type, Position position) {
        super("Vehicule", position);
        this.type = type;
        this.nuagesCaptures = 0;
    }

    @Override
    public void deplacer(Position nouvellePosition) {
        this.position = nouvellePosition;
    }

    @Override
    public String toString() {
        if (type.equalsIgnoreCase("Methane")) {
            return "VM";
        } else if (type.equalsIgnoreCase("Eau")) {
            return "VE";
        } else {
            return "V";
        }
    }

    public String getType() {
        return type;
    }

    public int getNuagesCaptures() {
        return nuagesCaptures;
    }

    public void capturerNuage() {
        nuagesCaptures++;
    }

    public void perdreNuagesCaptures(int nuagesPerdus) {
    this.nuagesCaptures -= nuagesPerdus;
    if (this.nuagesCaptures < 0) {
        this.nuagesCaptures = 0;
    }
}

    public boolean estActive() {
        return nuagesCaptures >= 1 && nuagesCaptures <= 3;
    }

    public void resetNuagesCaptures() {
        nuagesCaptures = 0;
    }

    public void activer() {
        nuagesCaptures = 0;
    }
}

