package Projet.Pieces;

import Projet.Plateau.Position;

public class Nuage extends Piece {
    private String type;

    public Nuage(String type, Position position) {
        super("Nuage", position);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void deplacer(Position nouvellePosition) {
        this.position = nouvellePosition;
    }

    @Override
    public String toString() {
        if (type.equals("Methane")) {
            return "NM";  
        } else {
            return "NE";
        }
    }
}




