package Projet.Plateau;

import java.io.*;

public class Coup implements Serializable {
    public static final long serialVersionUID = 1L;
    private String mouvement;
    private String resultat;

    public Coup(String mouvement, String resultat) {
        this.mouvement = mouvement;
        this.resultat = resultat;
    }

    public String getMouvement() {
        return mouvement;
    }

    public String getResultat() {
        return resultat;
    }

    @Override
    public String toString() {
        return mouvement + ". " + resultat;
    }
}


