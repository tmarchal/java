package Projet.Plateau;

import java.util.ArrayList;
import java.util.List;
import Projet.Pieces.Piece;
import Projet.Pieces.Vehicule;
import Projet.Pieces.Nuage;
import Projet.Pieces.Glace;
import java.io.Serializable;

public class Joueur implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Piece> pieces;
    private String nom;
    private String couleur;

    public Joueur(String nom) {
        this.nom = nom;
        this.pieces = new ArrayList<>();
    }

    public Joueur() {
        this("Joueur"); 
        this.pieces = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public Piece TrouverPiecePosition(Position position) {
        for (Piece piece : pieces) {
            if (piece.getPosition().equals(position)) {
                return piece;
            }
        }
        return null;
    }

    public boolean PiecePosition(Position position) {
        for (Piece piece : pieces) {
            if (piece.getPosition().equals(position)) {
                return true;
            }
        }
        return false;
    }

    public void ajouterPiece(Piece piece, Position position, Piece[][] grille) {
        if (piece != null && position != null && grille != null &&
                position.getX() >= 0 && position.getX() < grille.length &&
                position.getY() >= 0 && position.getY() < grille[0].length) {
            piece.setPosition(position);
            pieces.add(piece);
            grille[position.getX()][position.getY()] = piece;
        }
    }

    public List<Piece> AvoirPieces() {
        return pieces;
    }


    public void deplacerPiece(Piece piece, Position nouvellePosition) {
        if (pieces.contains(piece)) {
            piece.deplacer(nouvellePosition);
        }
    }


    public void supprimerPiece(Piece piece) {
        pieces.remove(piece);
    }

    public boolean possedeVehicule() {
        for (Piece piece : pieces) {
            if (piece instanceof Vehicule) {
                return true;
            }
        }
        return false;
    }

    public boolean possedeGlace() {
        for (Piece piece : pieces) {
            if (piece instanceof Glace) {
                return true;
            }
        }
        return false;
    }

    public boolean possedeNuage() {
        for (Piece piece : pieces) {
            if (piece instanceof Nuage) {
                return true;
            }
        }
        return false;
    }


    public int compterNuagesDansVehicules() {
        int count = 0;
        for (Piece piece : pieces) {
            if (piece instanceof Vehicule) {
                Vehicule vehicule = (Vehicule) piece;
                count += vehicule.getNuagesCaptures();
            }
        }
        return count;
    }
}
