package Projet.Plateau;

import Projet.Pieces.Vehicule;
import Projet.Pieces.Glace;
import Projet.Pieces.Nuage;
import Projet.Pieces.Piece;
import Projet.Plateau.Joueur;
import Projet.Plateau.Coup;
import java.util.Random;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.io.*;

public class Plateau implements Serializable{
    private static final long serialVersionUID = 1L;
    private Piece[][] grille;
    private int tailleX;
    private int tailleY;
    private Joueur joueur1;
    private Joueur joueur2;
    private Joueur joueurActuel;
    public List<Piece> piecesJoueur1;
    public List<Piece> piecesJoueur2;
    private List<Coup> coupsJoues = new ArrayList<>();


    public Plateau(int tailleX, int tailleY) {
        this.tailleX = tailleX;
        this.tailleY = tailleY;
        this.grille = new Piece[tailleX][tailleY];
    }

    public void ajouterPieces(Joueur joueur1, Joueur joueur2) {
        this.joueur1 = joueur1;
        this.joueur2 = joueur2;
        placerGlaces(joueur1, joueur2, grille);
        placerVehicules();
        placerNuages();

        joueurActuel = joueur1;
    }

    public void afficherPlateau() {
    System.out.print("   ");
    for (char c = 'A'; c < 'A' + tailleY; c++) {
        System.out.print(""+c + "   ");
    }
    System.out.println();

    for (int i = 0; i < tailleX; i++) {
        System.out.print((i + 1) + " ");
        for (int j = 0; j < tailleY; j++) {
            Piece piece = grille[i][j];
            if (piece != null) {
                System.out.print("[" + piece + "]");
            } else {
                System.out.print("[  ]");
            }
        }
        System.out.println();
    }
}

    public void tourDeJeu(Joueur joueur1, Joueur joueur2, Scanner scanner) {
        afficherPlateau();
        System.out.println(joueurActuel.getNom() + ", c'est à vous de jouer.");
        System.out.println("Choisissez la position de la pièce à jouer (de type A1):");
        String inputPiece = scanner.next();
        System.out.println("Choisissez sa nouvelle position :");
        String inputNouvellePosition = scanner.next();

        Position positionPiece = convertirPosition(inputPiece);
        Position nouvellePosition = convertirPosition(inputNouvellePosition);

        if (positionPiece != null && nouvellePosition != null) {
            Piece piece = getPieceFromCoordinates(positionPiece.getX(), positionPiece.getY());
            if (piece != null) {
                if (!joueurActuel.PiecePosition(positionPiece)) {
                    System.out.println("Vous ne pouvez déplacer que vos propres pièces. Réessayez.");
                    tourDeJeu(joueur1,joueur2,scanner);
                }
                if (piece instanceof Nuage || (piece instanceof Vehicule && ((Vehicule) piece).estActive())) {
                    DeplacementDouble(piece, nouvellePosition);
                } else {
                    if (VerifDeplacement(piece, nouvellePosition)) {
                        DeplacementSimple(piece, nouvellePosition);
                    } else {
                        System.out.println("Déplacement invalide. Réessayez.");
                        tourDeJeu(joueur1,joueur2,scanner);

                    }
                }
                joueurActuel = (joueurActuel == joueur1) ? joueur2 : joueur1;
            } else {
                System.out.println("Pas de pièce sur cette case. Réessayez.");
                tourDeJeu(joueur1,joueur2,scanner);
            }
        } else {
            System.out.println("Position invalide. Réessayez.");
            tourDeJeu(joueur1,joueur2,scanner);
        }
    }

    public void tourDeJeuOrdi(Joueur joueur1, Joueur joueur2, Scanner scanner) {
        afficherPlateau();
        System.out.println(joueurActuel.getNom() + ", c'est à vous de jouer.");
        System.out.println("Choisissez la position de la pièce à jouer (de type A1):");
        String inputPiece = scanner.next();
        System.out.println("Choisissez sa nouvelle position :");
        String inputNouvellePosition = scanner.next();

        Position positionPiece = convertirPosition(inputPiece);
        Position nouvellePosition = convertirPosition(inputNouvellePosition);

        if (positionPiece != null && nouvellePosition != null) {
            Piece piece = getPieceFromCoordinates(positionPiece.getX(), positionPiece.getY());
            if (piece != null) {
                if (!joueurActuel.PiecePosition(positionPiece)) {
                    System.out.println("Vous ne pouvez déplacer que vos propres pièces. Réessayez.");
                    return;
                }
                if (piece instanceof Nuage || (piece instanceof Vehicule && ((Vehicule) piece).estActive())) {
                    DeplacementDouble(piece, nouvellePosition);
                } else {
                    if (VerifDeplacement(piece, nouvellePosition)) {
                        DeplacementSimple(piece, nouvellePosition);
                    } else {
                        System.out.println("Déplacement invalide. Réessayez.");
                    }
                }
                joueurActuel = joueur1;
            } else {
                System.out.println("Pas de pièce sur cette case. Réessayez.");
            }
        } else {
            System.out.println("Position invalide. Réessayez.");
        }
    }

   public void deplacerNuagesAleatoirement() {
    Random random = new Random();
    for (int i = 0; i < tailleX; i++) {
        for (int j = 0; j < tailleY; j++) {
            if (grille[i][j] instanceof Nuage) {
                if (random.nextInt(5) == 0) {
                    int direction = random.nextInt(4);
                    int newX = i, newY = j;
                    switch (direction) {
                        case 0:
                            System.out.println("0");
                            newX = Math.max(0, i - 2);
                            break;
                        case 1:
                            System.out.println("1");
                            newX = Math.min(tailleX - 1, i + 2);
                            break;
                        case 2:
                            System.out.println("2");
                            newY = Math.max(0, j - 2);
                            break;
                        case 3:
                            newY = Math.min(tailleY - 1, j + 2);
                            break;
                    }
                    if (grille[newX][newY] == null || grille[newX][newY] instanceof Piece) {
                        if (grille[newX][newY] instanceof Piece) {
                            System.out.println("Le nuage a éliminé " + grille[newX][newY] + " à la position (" + newX + "," + newY + ")");
                        }
                        grille[newX][newY] = grille[i][j];
                        grille[i][j] = null;
                        ((Nuage) grille[newX][newY]).deplacer(new Position(newX, newY));
                    }
                }
            }
        }
    }
}

    private void placerGlaces(Joueur joueur1, Joueur joueur2,Piece[][] grille) {
        for (int i = 0; i < tailleY; i++) {
            if (i % 2 == 0) {
                Glace glace1 = new Glace("G", new Position(0, i));
                Glace glace2 = new Glace("G", new Position(3, i));
                joueur1.ajouterPiece(glace1, glace1.getPosition(),grille);
                joueur1.ajouterPiece(glace2, glace2.getPosition(),grille);

                Glace glace3 = new Glace("G", new Position(tailleX - 1, i));
                Glace glace4 = new Glace("G", new Position(tailleX - 4, i));
                joueur2.ajouterPiece(glace3, glace3.getPosition(),grille);
                joueur2.ajouterPiece(glace4, glace4.getPosition(),grille);
            }
            if (i % 2 == 1) {
                Glace glace5 = new Glace("G", new Position(2, i));
                joueur1.ajouterPiece(glace5, glace5.getPosition(),grille);

                Glace glace6 = new Glace("G", new Position(tailleX - 3, i));
                joueur2.ajouterPiece(glace6, glace6.getPosition(),grille);
            }
        }
    }

    private void placerVehicules() {
        Random random = new Random();
        int methaneCount = 0;
        int waterCount = 0;
        int methaneCount2 = 0;
        int waterCount2 = 0;

        for (int i = 0; i < 16; i++) {
            String type;
            if (i % 2 == 0) {
                if (methaneCount < 4 && waterCount < 4) {
                    double randomValue = random.nextDouble();
                    if (randomValue < 0.5) {
                        type = "Methane";
                        methaneCount++;
                    } else {
                        type = "Eau";
                        waterCount++;
                    }
                } else if (methaneCount < 4) {
                    type = "Methane";
                    methaneCount++;
                } else {
                    type = "Eau";
                    waterCount++;
                }
                Vehicule vehicule1 = new Vehicule(type, new Position(1, i));
                Vehicule vehicule2 = new Vehicule(type, new Position(tailleX - 2, i));

                joueur1.ajouterPiece(vehicule1, vehicule1.getPosition(),grille);
                joueur2.ajouterPiece(vehicule2, vehicule2.getPosition(),grille);
            }
        }
    }

    private void placerNuages() {
        Random random = new Random();
        List<Position> positions = new ArrayList<>();
        for (int i = 4; i < tailleX - 4; i++) {
            for (int j = 0; j < tailleY; j++) {
                positions.add(new Position(i, j));
            }
        }
        Collections.shuffle(positions);

        int methaneCount = 0;
        int waterCount = 0;
        for (Position position : positions) {
            if (methaneCount < 15 && waterCount < 15) {
                double randomValue = random.nextDouble();
                String type;
                if (randomValue < 0.5) {
                    type = "Methane";
                    methaneCount++;
                } else {
                    type = "Eau";
                    waterCount++;
                }
                Nuage nuage1 = new Nuage(type, position);
                Nuage nuage2 = new Nuage(type, position);

                joueur1.ajouterPiece(nuage1, nuage1.getPosition(),grille);
                joueur2.ajouterPiece(nuage2, nuage2.getPosition(),grille);
            } else if (methaneCount < 15) {
                Nuage nuage = new Nuage("Methane", position);
                joueur1.ajouterPiece(nuage, nuage.getPosition(),grille);
                joueur2.ajouterPiece(nuage, nuage.getPosition(),grille);
                methaneCount++;
            } else if (waterCount < 15) {
                Nuage nuage = new Nuage("Eau", position);
                joueur1.ajouterPiece(nuage, nuage.getPosition(),grille);
                joueur2.ajouterPiece(nuage, nuage.getPosition(),grille);
                waterCount++;
            }
        }
    }

    private Piece getPieceFromCoordinates(int x, int y) {
        Position position = new Position(x, y);
        return TrouverPiecePosition(position);
    }

    private Piece TrouverPiecePosition(Position position) {
        return grille[position.getX()][position.getY()];
    }

    private Piece TrouverPiecePosition(Position position, List<Piece> piecesJoueur1, List<Piece> piecesJoueur2) {
        for (Piece piece : piecesJoueur1) {
            if (piece.getPosition().equals(position)) {
                return piece;
            }
        }
        for (Piece piece : piecesJoueur2) {
            if (piece.getPosition().equals(position)) {
                return piece;
            }
        }
        return null;
    }

    private Position convertirPosition(String input) {
        if (input.length() < 2 || input.length() > 3) {
            return null;
        }

        char colChar = input.charAt(0);
        String rowStr = input.substring(1);

        if (colChar < 'A' || colChar >= 'A' + tailleY) {
            return null;
        }

        int col = colChar - 'A';
        int row;

        try {
            row = Integer.parseInt(rowStr) - 1;
        } catch (NumberFormatException e) {
            return null;
        }

        if (row < 0 || row >= tailleX) {
            return null;
        }

        return new Position(row, col);
    }

    public String convertirPosition(int x, int y) {
    if (x < 0 || x >= tailleX || y < 0 || y >= tailleY) {
        return null;
    }

    char colChar = (char) ('A' + y);
    int row = x + 1;

    return String.valueOf(colChar) + row;
    } 

    private String convertirCoordonnees(Position position) {
    char col = (char) ('A' + position.getY());
    int row = position.getX() + 1;
    return "" + col + row;
}


    public boolean VerifDeplacement(Piece piece, Position nouvellePosition) {
        int newRow = nouvellePosition.getX();
        int newCol = nouvellePosition.getY();

        int currentRow = piece.getPosition().getX();
        int currentCol = piece.getPosition().getY();

        int diffRow = Math.abs(newRow - currentRow);
        int diffCol = Math.abs(newCol - currentCol);

        boolean isHorizontalMove = diffRow == 0 && diffCol == 1;
        boolean isVerticalMove = diffRow == 1 && diffCol == 0;
        boolean isInfiny = diffRow == 15 && diffCol == 0;
        boolean isInfiny2 = diffRow == 0 && diffCol == 15;

        if (isHorizontalMove || isVerticalMove || isInfiny || isInfiny2) {
            Piece pieceAtNewPosition = TrouverPiecePosition(nouvellePosition);
            return pieceAtNewPosition == null || pieceAtNewPosition instanceof Glace ||
                    pieceAtNewPosition instanceof Vehicule || pieceAtNewPosition instanceof Nuage;
        }
        return false;
    }

    public boolean deplacerPiece(Piece piece, Position nouvellePosition, List<Piece> piecesJoueur1, List<Piece> piecesJoueur2) {
        if (piece != null) {
            Position anciennePosition = piece.getPosition();
            Piece pieceAtNewPosition = TrouverPiecePosition(nouvellePosition);

            if (piece instanceof Nuage || piece instanceof Glace) {
            if (pieceAtNewPosition != null) {
                System.out.println("Le nuage/glace a éliminé la pièce " + pieceAtNewPosition);
                
                if (pieceAtNewPosition instanceof Vehicule) {
                    Vehicule vehiculeDetruit = (Vehicule) pieceAtNewPosition;
                    int nuagesPerdus = vehiculeDetruit.getNuagesCaptures();
                    vehiculeDetruit.perdreNuagesCaptures(nuagesPerdus);
                }
                
                String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
                coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
                grille[anciennePosition.getX()][anciennePosition.getY()] = null;
                grille[nouvellePosition.getX()][nouvellePosition.getY()] = piece;
                piece.deplacer(nouvellePosition);
                return true;
            }
        }

            if (piece instanceof Vehicule && pieceAtNewPosition instanceof Nuage &&
                    ((Vehicule) piece).getType().equalsIgnoreCase(((Nuage) pieceAtNewPosition).getType())) {
                Vehicule vehicule = (Vehicule) piece;
                Nuage nuage = (Nuage) pieceAtNewPosition;

                if (vehicule.getNuagesCaptures() < 3) {
                    System.out.println("Le véhicule a capturé le nuage de même type !");
                    vehicule.capturerNuage();
                    grille[anciennePosition.getX()][anciennePosition.getY()] = null;
                    grille[nouvellePosition.getX()][nouvellePosition.getY()] = vehicule;
                    vehicule.deplacer(nouvellePosition);
                    String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
                    coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
                    return true;
                } else {
                    System.out.println("Le véhicule ne peut pas capturer plus de nuages !");
                    grille[nouvellePosition.getX()][nouvellePosition.getY()] = null;
                    String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
                    coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
                    return true;
                }
            }

            if (piece instanceof Vehicule && pieceAtNewPosition instanceof Nuage &&
                    !((Vehicule) piece).getType().equalsIgnoreCase(((Nuage) pieceAtNewPosition).getType())) {
                System.out.println("Le véhicule a été éliminé par un nuage de type différent !");
                grille[anciennePosition.getX()][anciennePosition.getY()] = null;
                grille[pieceAtNewPosition.getPosition().getX()][pieceAtNewPosition.getPosition().getY()] = pieceAtNewPosition;
                String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
                coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
                Vehicule vehiculeDetruit = (Vehicule) piece;
                int nuagesPerdus = vehiculeDetruit.getNuagesCaptures();
                vehiculeDetruit.perdreNuagesCaptures(nuagesPerdus);
                return true;
            }

            if (piece instanceof Vehicule && pieceAtNewPosition instanceof Glace) {
                System.out.println("Le véhicule a été éliminé par la glace !");
                grille[anciennePosition.getX()][anciennePosition.getY()] = null;
                grille[pieceAtNewPosition.getPosition().getX()][pieceAtNewPosition.getPosition().getY()] = pieceAtNewPosition;
                Vehicule vehiculeDetruit = (Vehicule) piece;
                int nuagesPerdus = vehiculeDetruit.getNuagesCaptures();
                vehiculeDetruit.perdreNuagesCaptures(nuagesPerdus);
                String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
                coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
                return true;
            }
            grille[anciennePosition.getX()][anciennePosition.getY()] = null;
            grille[nouvellePosition.getX()][nouvellePosition.getY()] = piece;
            String coupString = convertirCoordonnees(anciennePosition) + "-" + convertirCoordonnees(nouvellePosition);
            coupsJoues.add(new Coup(coupString, joueur1.compterNuagesDansVehicules() + "-" + joueur2.compterNuagesDansVehicules()));
            piece.deplacer(nouvellePosition);
            return true;

        } else {
            System.out.println("La pièce fournie est null. Déplacement annulé.");
            return false;
        }
    }

    private void DeplacementSimple(Piece piece, Position nouvellePosition) {
        if (piece != null) {
            Position anciennePosition = piece.getPosition();
            int deltaX = nouvellePosition.getX() - anciennePosition.getX();
            int deltaY = nouvellePosition.getY() - anciennePosition.getY();

            if ((deltaX == 1 || deltaX == -1) && deltaY == 0) {
                deplacerPiece(piece, nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else if ((deltaY == 1 || deltaY == -1) && deltaX == 0) {
                deplacerPiece(piece, nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else {
                System.out.println("Déplacement en diagonale interdit.");
            }
            if ((deltaX==-15 || deltaX==15) && deltaY==0 || deltaX==0 && (deltaY==15 ||deltaY==-15)){
                deplacerPiece(piece,nouvellePosition,piecesJoueur1,piecesJoueur2);
            }
        } else {
            System.out.println("La pièce ou la position est invalide. Déplacement annulé.");
        }
    }

    private void DeplacementDouble(Piece piece, Position nouvellePosition) {
        if (piece != null) {
            Position anciennePosition = piece.getPosition();
            int deltaX = nouvellePosition.getX() - anciennePosition.getX();
            int deltaY = nouvellePosition.getY() - anciennePosition.getY();

            int surplombX = anciennePosition.getX() + (deltaX / 2);
            int surplombY = anciennePosition.getY() + (deltaY / 2);
            Piece pieceSurplombee = TrouverPiecePosition(new Position(surplombX, surplombY));

            if ((deltaX == 2 || deltaX == -2) && deltaY == 0) {
                deplacerPiece(piece, nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else if ((deltaY == 2 || deltaY == -2) && deltaX == 0) {
                deplacerPiece(piece, nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else if ((deltaY == 2 || deltaY == -2) && (deltaX == 2 || deltaY ==-2)) {
                deplacerPiece(piece, nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else if 
            (((deltaX==-14 || deltaX==14) &&  deltaY==0) || (deltaX==0 && (deltaY==14 ||deltaY==-14))
            || ((deltaX==-14 || deltaX==14) &&  (deltaY==-2 || deltaY==2)) || ((deltaX==2 || deltaX==-2) && (deltaY==14 ||deltaY==-14))){
                deplacerPiece(piece,nouvellePosition,piecesJoueur1,piecesJoueur2);
            } else {
                System.out.println("Déplacement interdit.");
                return;
            }

            if (pieceSurplombee instanceof Glace) {
                System.out.println("Déplacement aérien impossible. Case en dessous est une glace.");
                return;
            }
        }
    }

    public boolean VerifNuagesCollectes() {
        for (int i = 0; i < tailleX; i++) {
            for (int j = 0; j < tailleY; j++) {
                if (grille[i][j] instanceof Nuage) {
                    return false;
                }
            }
        }
        return true;
    }

    public void enregistrerCoup(String mouvement, String resultat) {
    coupsJoues.add(new Coup(mouvement, resultat));
}
    public void afficherCoupsJoues() {
        System.out.println("Historique des coups joués :");
        for (int i = 0; i < coupsJoues.size(); i++) {
            System.out.println((i + 1) + ". " + coupsJoues.get(i));
        }
    }

    public Joueur getJoueur1() {
        return joueur1;
    }

    public Joueur getJoueur2() {
        return joueur2;
    }
}


